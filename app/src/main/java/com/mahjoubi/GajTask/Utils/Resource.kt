package com.mahjoubi.GajTask.Utils

data class Resource<out T , out G>(val status: Status, val Citydata: T?, val Fooddata: G?, val message: String?) {

    companion object {

        fun <T , G> success(Citydata: T? , Fooddata:G?): Resource<T, G> {
            return Resource(Status.SUCCESS, Citydata,Fooddata, null)
        }

        fun <T , G> error(msg: String, Citydata: T? , Fooddata:G?): Resource<T, G> {
            return Resource(Status.ERROR, Citydata , Fooddata, msg)
        }

        fun <T , G> loading(Citydata: T? , Fooddata:G?): Resource<T, G> {
            return Resource(Status.LOADING, Citydata,Fooddata, null)
        }

    }

}