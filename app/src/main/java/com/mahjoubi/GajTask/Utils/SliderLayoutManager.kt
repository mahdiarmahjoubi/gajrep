package com.mahjoubi.GajTask.Utils

import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.content.Context
import android.os.Build
import android.view.Gravity
import android.view.View
import android.widget.TextSwitcher
import android.widget.TextView
import android.widget.ViewSwitcher
import androidx.annotation.StyleRes
import androidx.recyclerview.widget.RecyclerView
import com.mahjoubi.GajTask.View.adapter.DataAdapter
import com.mahjoubi.GajTask.R
import com.mahjoubi.GajTask.data.local.entity.CityModelEntity
import com.ramotion.cardslider.CardSliderLayoutManager
import com.ramotion.cardslider.CardSnapHelper

class SliderLayoutManager(
    private val context: Context,
    private val recyclerView: RecyclerView,
    private val content: List<CityModelEntity>,
    private val title: TextView,
    private val nextTitle: TextView,
    private val description: TextSwitcher?
) {

    private var layoutManger: CardSliderLayoutManager? = null
    private val descriptions = mutableListOf<String>()
    private val titles = mutableListOf<String>()
    private var countryOffset1 = 0
    private var countryOffset2 = 0
    private var countryAnimDuration: Long = 0
    private var currentPosition = 0


    init {
        layoutManger = recyclerView.layoutManager as CardSliderLayoutManager?
        content.forEach {
            it.description?.let { it1 -> descriptions.add(it1) }
        }
        content.forEach {
            it.name?.let { it1 -> titles.add(it1) }
        }

        initCountryText()

        initSwitchers()
    }


    private inner class TextViewFactory internal constructor(
        @field:StyleRes @param:StyleRes val styleId: Int,
        val center: Boolean
    ) : ViewSwitcher.ViewFactory {
        override fun makeView(): View {
            val textView = TextView(context)
            if (center) {
                textView.gravity = Gravity.CENTER
            }
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
                textView.setTextAppearance(context, styleId)
            } else {
                textView.setTextAppearance(styleId)
            }
            return textView
        }
    }

    private fun initSwitchers() {

        description?.let {
            it.setInAnimation(context, android.R.anim.fade_in)
            it.setOutAnimation(context, android.R.anim.fade_out)
            it.setFactory(TextViewFactory(R.style.DescriptionTextView, false))
            it.setCurrentText(content[0].description)

        }


    }


    private fun initCountryText() {
        countryAnimDuration =
            context.resources.getInteger(R.integer.labels_animation_duration).toLong()
        countryOffset1 = context.resources.getDimensionPixelSize(R.dimen.left_offset)
        countryOffset2 = context.resources.getDimensionPixelSize(R.dimen.card_width)

        title!!.x = countryOffset1.toFloat()
        nextTitle!!.x = countryOffset2.toFloat()
        title!!.text = content[0].name
        nextTitle!!.alpha = 0f

    }


    private fun setCountryText(text: String, left2right: Boolean) {
        val invisibleText: TextView?
        val visibleText: TextView?
        if (title!!.alpha > nextTitle!!.alpha) {
            visibleText = title
            invisibleText = nextTitle
        } else {
            visibleText = nextTitle
            invisibleText = title
        }
        val vOffset: Int
        if (left2right) {
            invisibleText!!.x = 0f
            vOffset = countryOffset2
        } else {
            invisibleText!!.x = countryOffset2.toFloat()
            vOffset = 0
        }
        invisibleText.text = text
        val iAlpha = ObjectAnimator.ofFloat(invisibleText, "alpha", 1f)
        val vAlpha = ObjectAnimator.ofFloat(visibleText, "alpha", 0f)
        val iX = ObjectAnimator.ofFloat(invisibleText, "x", countryOffset1.toFloat())
        val vX = ObjectAnimator.ofFloat(visibleText, "x", vOffset.toFloat())
        val animSet = AnimatorSet()
        animSet.playTogether(iAlpha, vAlpha, iX, vX)
        animSet.duration = countryAnimDuration
        animSet.start()
    }

    private fun onActiveCardChange() {
        val pos = layoutManger!!.activeCardPosition
        if (pos == RecyclerView.NO_POSITION || pos == currentPosition) {
            return
        }

        onActiveCardChange(pos)
    }


    fun onActiveCardChange(pos: Int) {
        val animH = intArrayOf(R.anim.slide_in_right, R.anim.slide_out_left)
        val animV = intArrayOf(R.anim.slide_in_top, R.anim.slide_out_bottom)
        val left2right = pos < currentPosition
        if (left2right) {
            animH[0] = android.R.anim.slide_in_left
            animH[1] = android.R.anim.slide_out_right
            animV[0] = R.anim.slide_in_bottom
            animV[1] = R.anim.slide_out_top
        }
        setCountryText(titles[pos % titles.size], left2right)

        description?.let {
            description!!.setText(descriptions[pos % descriptions.size])
        }

        currentPosition = pos
    }

    fun setupLayout(adapter: DataAdapter) {

        recyclerView.adapter = adapter

        recyclerView!!.setHasFixedSize(true)
        recyclerView!!.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                    onActiveCardChange()
                }
            }
        })

        CardSnapHelper().attachToRecyclerView(recyclerView)

        adapter.addData(content)
        adapter.notifyDataSetChanged()
    }


    fun getContent(): List<CityModelEntity> {

        return content;

    }


}