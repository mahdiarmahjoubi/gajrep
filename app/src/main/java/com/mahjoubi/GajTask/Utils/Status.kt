package com.mahjoubi.GajTask.Utils

enum class Status {

    SUCCESS,
    ERROR,
    LOADING
}