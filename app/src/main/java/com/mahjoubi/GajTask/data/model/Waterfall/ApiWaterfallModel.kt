package com.mahjoubi.mediatoradlibrary.data.model.Waterfall

import com.google.gson.annotations.SerializedName

data class ApiWaterfallModel(

    @SerializedName("id")
    val id: String = "",
    @SerializedName("name")
    val name: String = "",

)