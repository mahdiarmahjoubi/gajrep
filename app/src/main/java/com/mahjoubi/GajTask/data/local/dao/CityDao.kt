package com.mahjoubi.GajTask.data.local.dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import com.mahjoubi.GajTask.data.local.entity.CityModelEntity

@Dao
interface CityDao {


    @Query("SELECT * FROM CityModelEntity")
    suspend fun getAll(): List<CityModelEntity>

    @Insert
    suspend fun insertAll(users: List<CityModelEntity>)

    @Delete
    suspend fun delete(user: CityModelEntity)
}