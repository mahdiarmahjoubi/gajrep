package com.mahjoubi.GajTask.data.local.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity
data class CityModelEntity(
    @PrimaryKey val id: Int,
    @ColumnInfo(name = "type") val type: String?,
    @ColumnInfo(name = "name") val name: String?,
    @ColumnInfo(name = "image") val image: String?,
    @ColumnInfo(name = "description") val description: String?,

    )
