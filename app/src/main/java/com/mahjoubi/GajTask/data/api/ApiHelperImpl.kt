package com.mahjoubi.GajTask.data.api

import com.mahjoubi.mediatoradlibrary.data.model.Waterfall.ApiModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class ApiHelperImpl(private val apiService: ApiService) : ApiHelper {


    override fun getData(): Flow<ApiModel> = flow {
        emit(apiService.getData()) }


}