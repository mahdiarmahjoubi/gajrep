package com.mahjoubi.GajTask.data.model.Waterfall

import com.google.gson.annotations.SerializedName

data class CityModel(

    @SerializedName("name")
    val name: String,
    @SerializedName("image")
    val image: String = "",
    @SerializedName("description")
    val description: String = ""
)
