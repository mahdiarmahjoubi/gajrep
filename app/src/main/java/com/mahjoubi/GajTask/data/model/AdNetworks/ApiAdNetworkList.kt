package com.mahjoubi.mediatoradlibrary.data.model.AdNetworks

import com.google.gson.annotations.SerializedName

data class ApiAdNetworkList(
    @SerializedName("adNetworks")
    val adNetworks: List<ApiAdNetworksModel> ,
)