package com.mahjoubi.GajTask.data.local

import com.mahjoubi.GajTask.data.local.entity.CityModelEntity
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class DatabaseHelperImpl(private val appDatabase: AppDatabase) : DatabaseHelper {

    override fun getlocaldata(): Flow<List<CityModelEntity>> =
        flow { emit(appDatabase.waterfallDao().getAll()) }

    override fun insertAll(users: List<CityModelEntity>): Flow<Unit> = flow {
        appDatabase.waterfallDao().insertAll(users)
        emit(Unit)
    }

}