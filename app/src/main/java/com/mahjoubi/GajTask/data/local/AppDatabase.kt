package com.mahjoubi.GajTask.data.local

import androidx.room.Database
import androidx.room.RoomDatabase
import com.mahjoubi.GajTask.data.local.dao.CityDao
import com.mahjoubi.GajTask.data.local.entity.CityModelEntity

@Database(entities = [CityModelEntity::class], version = 1)
abstract class AppDatabase : RoomDatabase() {

    abstract fun waterfallDao(): CityDao

}