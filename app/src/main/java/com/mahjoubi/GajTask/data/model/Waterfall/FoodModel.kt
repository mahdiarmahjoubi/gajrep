package com.mahjoubi.GajTask.data.model.Waterfall

import com.google.gson.annotations.SerializedName

data class FoodModel (

    @SerializedName("name")
    val name: String,
    @SerializedName("image")
    val image: String = ""
        )