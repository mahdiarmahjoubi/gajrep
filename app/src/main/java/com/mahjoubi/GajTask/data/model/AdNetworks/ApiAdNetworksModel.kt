package com.mahjoubi.mediatoradlibrary.data.model.AdNetworks

import com.google.gson.annotations.SerializedName

data class ApiAdNetworksModel(
    @SerializedName("id")
    val id: String = "",
    @SerializedName("name")
    val name: String = "",
)