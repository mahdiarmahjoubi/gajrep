package com.mahjoubi.GajTask.data.local

import com.mahjoubi.GajTask.data.local.entity.CityModelEntity
import kotlinx.coroutines.flow.Flow

interface DatabaseHelper {

    fun getlocaldata(): Flow<List<CityModelEntity>>

    fun insertAll(users: List<CityModelEntity>): Flow<Unit>

}