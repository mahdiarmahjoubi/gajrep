package com.mahjoubi.mediatoradlibrary.data.model.Waterfall

import com.google.gson.annotations.SerializedName
import com.mahjoubi.GajTask.data.model.Waterfall.CityModel
import com.mahjoubi.GajTask.data.model.Waterfall.FoodModel

data class ApiModel(

    @SerializedName("foods")
    val foods: List<FoodModel>,
    @SerializedName("cities")
    val cities: List<CityModel>,

    )