package com.mahjoubi.GajTask.data.api

import com.mahjoubi.mediatoradlibrary.data.model.Waterfall.ApiModel
import kotlinx.coroutines.flow.Flow

interface ApiHelper {


    fun getData(): Flow<ApiModel>

}