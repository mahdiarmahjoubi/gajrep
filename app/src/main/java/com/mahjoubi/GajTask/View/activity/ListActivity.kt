package com.mahjoubi.GajTask.View.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.RecyclerView
import com.mahjoubi.GajTask.View.adapter.DataAdapter
import com.mahjoubi.GajTask.R
import com.mahjoubi.GajTask.Utils.SliderLayoutManager
import com.mahjoubi.GajTask.Utils.Status
import com.mahjoubi.GajTask.ViewModel.RoomDBViewModel
import com.mahjoubi.GajTask.ViewModel.ViewModelFactory
import com.mahjoubi.GajTask.data.api.ApiHelperImpl
import com.mahjoubi.GajTask.data.api.RetrofitBuilder
import com.mahjoubi.GajTask.data.local.DatabaseBuilder
import com.mahjoubi.GajTask.data.local.DatabaseHelperImpl
import com.mahjoubi.GajTask.data.local.entity.CityModelEntity
import com.ramotion.cardslider.CardSliderLayoutManager
import kotlinx.android.synthetic.main.activity_main.*

class ListActivity : AppCompatActivity() {


    private lateinit var viewModel: RoomDBViewModel
    private lateinit var adapter: DataAdapter
    private lateinit var adapterFoods: DataAdapter



    var Cityslidermanger : SliderLayoutManager? = null
    var Foodsslidermanger : SliderLayoutManager? = null



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        adapter =
            DataAdapter(
                arrayListOf(),
                OnCardClickListener()
            )

        adapterFoods =
            DataAdapter(
                arrayListOf(),
                null
            )


        setupViewModel()
        setupObserver()
    }


    private fun setupObserver() {
        viewModel.getUsers().observe(this, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    progressBar.visibility = View.GONE

                    var Cities = mutableListOf<CityModelEntity>()
                    var Foods = mutableListOf<CityModelEntity>()
                    for(item in it.Citydata!!){
                        if (item.type == "Food"){
                            Foods.add(item)
                        }else{
                            Cities.add(item)
                        }
                    }

                    Cityslidermanger = SliderLayoutManager(
                        this@ListActivity,
                        recyclerView,
                        Cities ,
                        tv_country_1,
                        tv_country_2,
                        ts_description
                        )
                    Foodsslidermanger = SliderLayoutManager(
                        this@ListActivity,
                        recyclerView_foods,
                        Foods ,
                        tv_foods_1,
                        tv_foods_2,
                        null
                    )

                    Foodsslidermanger!!.setupLayout(adapterFoods)

                    Cityslidermanger!!.setupLayout(adapter)

                    recyclerView.visibility = View.VISIBLE
                }
                Status.LOADING -> {
                    progressBar.visibility = View.VISIBLE
                    recyclerView.visibility = View.GONE
                }
                Status.ERROR -> {
                    //Handle Error
                    progressBar.visibility = View.GONE
                    Toast.makeText(this, it.message, Toast.LENGTH_SHORT).show()
                }
            }
        })
    }



    private fun setupViewModel() {
        viewModel = ViewModelProviders.of(
            this,
            ViewModelFactory(
                ApiHelperImpl(RetrofitBuilder.apiService),
                DatabaseHelperImpl(DatabaseBuilder.getInstance(applicationContext))
            )
        ).get(RoomDBViewModel::class.java)
    }
    private inner class OnCardClickListener : View.OnClickListener {
        override fun onClick(view: View) {
            val lm: CardSliderLayoutManager? = recyclerView!!.layoutManager as CardSliderLayoutManager?
            if (lm != null) {
                if (lm.isSmoothScrolling()) {
                    return
                }
            }
            val activeCardPosition: Int = lm!!.getActiveCardPosition()
            if (activeCardPosition == RecyclerView.NO_POSITION) {
                return
            }
            val clickedPosition = recyclerView!!.getChildAdapterPosition(view)
            if (clickedPosition == activeCardPosition) {
                val intent = Intent(this@ListActivity, DetailActivity::class.java)
                intent.putExtra("description", Cityslidermanger!!.getContent()[clickedPosition].description)
                intent.putExtra("title", Cityslidermanger!!.getContent()[clickedPosition].name)
                intent.putExtra("imageUrl", Cityslidermanger!!.getContent()[clickedPosition].image)

                startActivity(intent)

            } else if (clickedPosition > activeCardPosition) {
                recyclerView!!.smoothScrollToPosition(clickedPosition)
                Cityslidermanger?.onActiveCardChange(clickedPosition)
            }
        }
    }






}