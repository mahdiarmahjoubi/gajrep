package com.mahjoubi.GajTask.View.activity

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.mahjoubi.GajTask.R
import kotlinx.android.synthetic.main.activity_main5.*

class DetailActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main5)

        val descriptionvalue:String = intent.getStringExtra("description").toString()
        val titlevalue:String = intent.getStringExtra("title").toString()
        val imagUrl:String = intent.getStringExtra("imageUrl").toString()


        title_tv.apply {
            text = titlevalue
        }

        desc_tv.apply {
            text = descriptionvalue
        }

        Glide.with(img.context)
            .load(imagUrl)
            .into(img)

        toolbar.apply {
            setNavigationOnClickListener(View.OnClickListener {
                startActivity(Intent(this@DetailActivity, ListActivity::class.java))
            })
        }


    }
}