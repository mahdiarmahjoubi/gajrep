package com.mahjoubi.GajTask.View.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.mahjoubi.GajTask.R
import com.mahjoubi.GajTask.data.local.entity.CityModelEntity
import kotlinx.android.synthetic.main.layout_slider_card.view.*

class DataAdapter(
    private val data: ArrayList<CityModelEntity> ,
    private val listener: View.OnClickListener?
) : RecyclerView.Adapter<DataAdapter.DataViewHolder>() {

    class DataViewHolder(itemView: View ) : RecyclerView.ViewHolder(itemView) {
        fun bind(data: CityModelEntity) {

            Glide.with(itemView.image.context)
                .load(data.image)
                .centerCrop()
                .into(itemView.image)


        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DataViewHolder {

        val view: View =   LayoutInflater.from(parent.context).inflate(
            R.layout.layout_slider_card, parent,
            false
        )
        if (listener != null) {
            view.setOnClickListener { view -> listener.onClick(view) }
        }
        return DataViewHolder(
          view
        )

    }


    override fun getItemCount(): Int = data.size

    override fun onBindViewHolder(holder: DataViewHolder, position: Int) =
        holder.bind(data[position])

    fun addData(list: List<CityModelEntity>) {
        data.addAll(list)
    }
}

