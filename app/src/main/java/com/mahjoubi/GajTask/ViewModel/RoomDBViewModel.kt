package com.mahjoubi.GajTask.ViewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.mahjoubi.GajTask.Utils.Resource
import com.mahjoubi.GajTask.data.api.ApiHelper
import com.mahjoubi.GajTask.data.local.DatabaseHelper
import com.mahjoubi.GajTask.data.local.entity.CityModelEntity
import com.mahjoubi.GajTask.data.model.Waterfall.FoodModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch

class RoomDBViewModel(private val apiHelper: ApiHelper, private val dbHelper: DatabaseHelper) :
    ViewModel() {

    private val CityandFoods = MutableLiveData<Resource<List<CityModelEntity>, List<FoodModel>>>()

    init {
       fetchUsers()
    }

    private fun fetchUsers() {
        viewModelScope.launch {
            CityandFoods.postValue(Resource.loading(null, null))
            dbHelper.getlocaldata()
                .flatMapConcat { dataFromDb ->
                    if (dataFromDb.isEmpty()) {
                        return@flatMapConcat apiHelper.getData()
                            .map { apimodelObservable ->


                                val userList = mutableListOf<CityModelEntity>()
                                for (city in apimodelObservable.cities) {
                                    val rnds = (0..1000).random()

                                    val temp = CityModelEntity(
                                        rnds,
                                        "City",
                                        city.name,
                                        city.image,
                                        city.description
                                    )
                                    userList.add(temp)
                                }
                                for (food in apimodelObservable.foods) {
                                    val rnds = (0..1000).random()

                                    val temp = CityModelEntity(
                                        rnds,
                                        "Food",
                                        food.name,
                                        food.image,
                                        ""
                                    )
                                    userList.add(temp)
                                }
                                userList
                            }
                            .flatMapConcat { modelsToDB ->
                                dbHelper.insertAll(modelsToDB)
                                    .flatMapConcat {
                                        flow {
                                            emit(modelsToDB)
                                        }
                                    }
                            }
                    } else {
                        return@flatMapConcat flow {
                            emit(dataFromDb)
                        }
                    }
                }
                .flowOn(Dispatchers.Default)
                .catch { e ->
                    CityandFoods.postValue(Resource.error(e.toString(), null, null))
                }
                .collect {
                    CityandFoods.postValue(Resource.success(it, null))
                }
        }
    }

    fun getUsers(): LiveData<Resource<List<CityModelEntity>, List<FoodModel>>> {
        return CityandFoods
    }

}